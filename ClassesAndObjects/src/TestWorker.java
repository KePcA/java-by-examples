
public class TestWorker {
	
	
	public static void main(String[] args) {
		
		Worker w1 = new Worker();
		
		w1.printAll();
		System.out.println();
		
		w1.setRegNumber(101);
		w1.setSurname("Bevk");
		w1.setName("Janko");
		w1.setNumberOfHours(182);
		
		System.out.println("Reg. Number: "+w1.getRegNumber());
		System.out.println("Surname and name: "+w1.getSurname()+" "+w1.getName());
		System.out.println("Number of hours: "+w1.getNumberOfHours());
		System.out.println();
		
		w1.printAll();
		
		System.out.println("Paycheck: "+w1.calcPaycheck(8.25)+" EUR");
		System.out.println();
		
		Worker w2 = new Worker(234,"Novak","Janez");
		w2.setNumberOfHours(174);
		w2.printAll();
		System.out.println("Paycheck: "+w2.calcPaycheck(12.5)+" EUR");
		System.out.println();
		
		Worker w3 = new Worker(314,"Tavcar","Zdenka",188);
		w3.printAll();
		System.out.println("Paycheck: "+w3.calcPaycheck(14.)+" EUR");
			
		
	}
	
}
