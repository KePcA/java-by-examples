
public class TestPoint {
	
	
	public static void main(String[] args) {
		
		
		Point p1 = new Point();
		Point p2 = new Point(2.0,5.5);
		Point p3 = new Point(4.0,45);
		
		p1.printCartesian();
		p1.printPolar();
		System.out.println();
		
		p2.printCartesian();
		p2.printPolar();
		System.out.println();
		
		p3.printCartesian();
		p3.printPolar();
		System.out.println();
		
		
		p1.setX(3.0);
		p1.setY(2.5);
		p1.printCartesian();
		p1.printPolar();
		System.out.println();
		
		p1.setPolar(0,0);
		p1.printCartesian();
		p1.printPolar();
		System.out.println();
		
		p1.move(3.0,2.5);
		p2.rotate(90);
		p3.extend(3.25);
		
		p1.printCartesian();
		p1.printPolar();
		System.out.println();
		p2.printCartesian();
		p2.printPolar();
		System.out.println();
		p3.printCartesian();
		p3.printPolar();
		System.out.println();
		
		System.out.printf("Distance p1 - p2: %.2f%n", p1.distance(p2));
		System.out.printf("Distance p2 - p1: %.2f%n", p2.distance(p1));
		System.out.printf("Distance p1 - p3: %.2f%n", p1.distance(p3));
		System.out.printf("Distance p2 - p3: %.2f%n", p2.distance(p3));
		
	}
	
}
