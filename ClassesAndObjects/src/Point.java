
public class Point {
	
	private double x;
	private double y;
	private double r;
	private int angle;
	
	
	
	public Point() {
		this.x = 0.0;
		this.y = 0.0;
		this.r = 0.0;
		this.angle = 0;
	}
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
		toPolar();	
	}
	
	public Point(double r, int angle) {
		this.r = r;
		this.angle = angle;
		toCart();
	}
	
	public void toPolar() {
		this.r = Math.sqrt(x*x + y*y);
		this.angle = (int)(Math.atan(y/x)*180/Math.PI);
	}
	
	public void toCart() {
		this.x = r*Math.cos(angle*Math.PI/180);
		this.y = r*Math.sin(angle*Math.PI/180);
	}
	
	
	public void setX(double x) {
		this.x = x;
		toPolar();
	}
	
	public void setY(double y) {
		this.y = y;
		toPolar();
	}
	
	public void setPolar(double r, int angle) {
		this.r = r;
		this.angle = angle;
		toCart();
	}
	
	public void move(double dx, double dy) {
		this.x += dx;
		this.y += dy;
		toPolar();
	}
	
	public void rotate(int alpha) {
		this.angle += alpha;
		toCart();
	}
	
	public void extend(double m) {
		this.r *= m;
		toCart();
	}
	
	public double distance(Point p1) {
		double dx = this.x - p1.x;
		double dy = this.y - p1.y;
		return Math.sqrt(dx*dx + dy*dy);
	}
	
	public void printCartesian() {
		System.out.printf("Point(x=%.2f, y=%.2f)%n",x,y);
	}
	
	public void printPolar() {
		System.out.printf("Point(r=%.2f, angle=%d)%n",r,angle);
	}
	
	
	

}
