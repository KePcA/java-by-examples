
public class TestShop {
	
	
	public static void main(String[] args) {
		
		Shop s = createShop();
		System.out.println(s.toString());
		System.out.println("Yearly plan is "+s.yearPlan()+".");
		System.out.println("Yearly sale is "+s.yearSale()+".");
		for(int m=0; m<6; m++) {
			System.out.print("Actual sale for "+(m+1)+". month: ");
			int d = ReadData.readInt();
			s.insertActualSale(d,m);
		}
		System.out.println(s.toString());
		
	}
	
	
	public static Shop createShop() {
		
		System.out.print("Insert code of the shop: ");
		int code = ReadData.readInt();
		System.out.print("Insert street and house number: ");
		String street = ReadData.readString();
		System.out.print("Insert town: ");
		String town = ReadData.readString();
		int[] plannedSale = new int[12];
		System.out.println("Insert planned sale: ");
		for(int m=0; m<12; m++) {
			System.out.print("- for "+(m+1)+". month: ");
			plannedSale[m] = ReadData.readInt();
		}
		return new Shop(code,street,town,plannedSale);
	}
	

}
