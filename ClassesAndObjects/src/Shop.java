
public class Shop {
	
	private int code;
	private String street;
	private String town;
	private int[] plannedSale;
	private int[] actualSale;
	
	
	public Shop(int code, String street, String town, int[] plannedSale) {
		this.code = code;
		this.street = street;
		this.town = town;
		this.plannedSale = new int[12];
		for(int month=0; month<12; month++) {
			this.plannedSale[month] = plannedSale[month];
		}
		this.actualSale = new int[12];
	}
	
	
	public void insertActualSale(int sale, int m) {
		this.actualSale[m] = sale;
	}
	
	public long yearPlan() {
		long sum = 0;
		for(int month=0; month<12; month++) {
			sum += plannedSale[month];
		}
		return sum;
	}
	
	public long yearSale() {
		long sum = 0;
		for(int month=0; month<12; month++) {
			sum += actualSale[month];
		}
		return sum;
	}
	
	
	public String toString() {
		String s = "\nShop " +code+"\n";
		s += "Address: "+street+", "+town+"\n";
		s += "Planned sale: ";
		for(int month=0; month<12; month++) {
			s += plannedSale[month]+" ";
		}
		s += "= "+yearPlan()+"\n";
		s += "Actual sale: ";
		for(int month=0; month<12; month++) {
			s += actualSale[month]+" ";
		}
		s += "= "+yearSale()+"\n";
		return s;
	}
	
	
	

}
