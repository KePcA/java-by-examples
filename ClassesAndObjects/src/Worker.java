
public class Worker {
	
	int regNumber;
	String surname;
	String name;
	int numberOfHours;
	
	
	public Worker() {
		
	}
	
	public Worker(int regNumber, String surname, String name) {
		this.regNumber = regNumber;
		this.surname = surname;
		this.name = name;
	}
	
	public Worker(int regNumber, String surname, String name, int numberOfHours) {
		this.regNumber = regNumber;
		this.surname = surname;
		this.name = name;
		this.numberOfHours = numberOfHours;
	}
	
	
	public void setRegNumber(int regNumber) {
		this.regNumber = regNumber;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setNumberOfHours(int numberOfHours) {
		this.numberOfHours = numberOfHours;
	}
	
	
	public int getRegNumber() {
		return regNumber;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public String getName() {
		return name;
	}
	
	public int getNumberOfHours() {
		return numberOfHours;
	}
	
	
	public double calcPaycheck(double hourPrice) {
		return numberOfHours*hourPrice;
	}
	
	public void printAll() {
		System.out.println("Registration number; "+regNumber);
		System.out.println("Surname and name: "+surname+" "+name);
		System.out.println("Number of hours: "+numberOfHours);
	}

}
