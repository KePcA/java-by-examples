
public class TestRectangle1 {
	
	
	public static void main(String[] args) {
		
		
		System.out.print("Number of rectangles: ");
		System.out.println(Rectangle1.returnNumberOfRectangles());
		
		Rectangle1 r1 = new Rectangle1();
		System.out.print("Number of rectangles: ");
		System.out.println(Rectangle1.returnNumberOfRectangles());
		
		Rectangle1 r2 = new Rectangle1(2.0,5.0,4.0,6.0);
		System.out.print("Number of rectangles: ");
		System.out.println(Rectangle1.returnNumberOfRectangles());
		
		System.out.println();
		
		r1.printRectangle();
		r2.printRectangle();
		
		r1.setLocation(2.0,2.0);
		r2.setLocation(7.0,0.0);
		r2.setSize(10.0,10.0);
		r2.move(3.0,2.0);
		
		r1.printRectangle();
		r2.printRectangle();
	}

}
