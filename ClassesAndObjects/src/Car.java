
public class Car {
	
	private String regNumber;
	private int highestSpeed;
	private double averageCons;
	private double fuelInTank;
	private int numberOfDrivenKM;
	
	
	public Car() {
		
	}
	
	public Car(String regNumber, int highestSpeed, double averageCons) {
		this.regNumber = regNumber;
		this.highestSpeed = highestSpeed;
		this.averageCons = averageCons;
		this.fuelInTank = 0.0;
		this.numberOfDrivenKM = 0;
	}
	
	public Car(String regNumber, int highestSpeed, double averageCons, double fuelInTank, int numberOfDrivenKM) {
		this.regNumber = regNumber;
		this.highestSpeed = highestSpeed;
		this.averageCons = averageCons;
		this.fuelInTank = fuelInTank;
		this.numberOfDrivenKM = numberOfDrivenKM;
	}
	
	
	public void setState(double fuelInTank, int numberOfDrivenKM) {
		this.fuelInTank = fuelInTank;
		this.numberOfDrivenKM = numberOfDrivenKM;
	}
	
	
	public double reach() {
		return fuelInTank/averageCons*100;
	}
	
	public int nextServis(int n) {
		return (numberOfDrivenKM/n+1)*n - numberOfDrivenKM;
	}
	
	public void printAll() {
		System.out.println("Reg number: "+regNumber);
		System.out.println("Highest Speed: "+highestSpeed);
		System.out.println("Average consumption: "+averageCons);
		System.out.println("Fuel in tank: "+fuelInTank);
		System.out.println("Number of km: "+numberOfDrivenKM);
		System.out.println();
		
		
	}
	

}
