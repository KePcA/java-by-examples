public class TestCar {
	
	
	public static void main(String[] args) {
		
		Car c1 = new Car("LJ A1 - 123", 200, 7.25);
		c1.printAll();
		
		c1.setState(70,23581);
		c1.printAll();
		
		Car c2 = new Car("KP M3-915", 165, 6.8, 28.9, 87345);
		c2.printAll();
		
		System.out.println("You can drive for the next "+c2.reach()+" km");
		System.out.println("Until next servis is "+c2.nextServis(15000)+" km");
		
	}

}
