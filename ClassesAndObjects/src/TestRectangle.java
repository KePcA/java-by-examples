
public class TestRectangle {
	
	
	public static void main(String[] args) {
		
		Rectangle r1 = new Rectangle();
		Rectangle r2 = new Rectangle(2.0,5.0,4.0,6.0);
		r1.printRectangle();
		r2.printRectangle();
		
		r1.setLocation(2.0,2.0);
		r2.setLocation(7.0,0.0);
		r2.setSize(10.0,10.0);
		r2.move(3.0,2.0);
		
		r1.printRectangle();
		r2.printRectangle();
	}
	

}
