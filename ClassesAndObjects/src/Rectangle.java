
public class Rectangle {
	
	private double x, y, a, b;
	
	
	
	public Rectangle() {
		this.x = 0.0;
		this.y = 1.0;
		this.a = 1.0;
		this.b = 1.0;
	}
	
	public Rectangle(double x, double y, double a, double b) {
		this.x = x;
		this.y = y;
		this.a = a;
		this.b = b;
	}
	
	public void setLocation(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public void setSize(double a, double b) {
		this.a = a;
		this.b = b;
	}
	
	public double calcCircum() {
		return 2*(a+b);
	}
	
	public double calcSize() {
		return a*b;
	}
	
	public void move(double dx, double dy) {
		this.x += dx;
		this.y += dy;
	}
	
	public void printRectangle() {
		System.out.printf("Rectangle(x=%.2f, y=%.2f, a=%.2f, b=%.2f)%n",x,y,a,b);
		System.out.printf("Circumference is %.2f.%n",calcCircum());
		System.out.printf("Size is %.2f.%n%n", calcSize());
	}
	
	
	
}
