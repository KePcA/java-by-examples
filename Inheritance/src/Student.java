
public class Student {
	
	private int regNo;
	private String surname;
	private String name;
	private int year;
	private int[] grades;
	
	
	public Student(int regNo, String surname, String name, int year, int noOfExams) {
		this.regNo = regNo;
		this.surname = surname;
		this.name = name;
		this.year = year;
		grades = new int[noOfExams];
		
	}
	
	//Inserts grade into an array
	public void insertGrade(int index, int grade) {
		grades[index] = grade;
	}
	
	public int getNoOfPassedExams() {
		int noOfPassed = 0;
		for(int i=0; i<grades.length; i++) {
			if(grades[i] >= 6) {
				noOfPassed++;
			}
		}
		return noOfPassed;
	}
	
	public boolean canAdvance(int noOfExamsToAdvance) {
		return getNoOfPassedExams() >= noOfExamsToAdvance;
				
	}
	
	public void printAll() {
		System.out.println("Reg number: "+regNo);
		System.out.println("Student: "+surname+" "+name);
		System.out.println("Year: "+year);
		System.out.print("Grades: ");
		for(int i=0; i<grades.length; i++) {
			System.out.print(grades[i]+" ");
		}
		System.out.println();
	}
	
	

}
