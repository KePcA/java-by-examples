
public abstract class Athlete {
	
	private int startNo;
	private String surname;
	private String name;
	private String club;
	
	public Athlete(int startNo, String surname, String name, String club) {
		this.startNo = startNo;
		this.surname = surname;
		this.name = name;
		this.club = club;
	}
	
	public abstract void enterResult();
	
	public String toString() {
		String s = "Start number: "+startNo+"\n";
		s += "Athlete: "+surname+" "+name+"\n";
		s += "Club: "+club+"\n";
		return s;
	}
	
	

}
