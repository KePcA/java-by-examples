
public class Vehicle {
	
	private String regNo;
	private String manufacturer;
	private String model;
	
	
	public Vehicle(String regNo, String manufacturer, String model) {
		this.regNo = regNo;
		this.manufacturer = manufacturer;
		this.model = model;
	}
	
	
	public void printData() {
		System.out.println("Reg. number: "+regNo);
		System.out.println("Manufacturer: "+manufacturer);
		System.out.println("Model: "+model);
	}

}
