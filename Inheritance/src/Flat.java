
public class Flat extends RealEstate {
	
	private double surfaceOfLP;
	private double surfaceOfOS;
	private double factor;
	private static double priceOfM2;
	
	
	public Flat(int noOfRealEstate, String surnameNameOfOwner, int builtYear, double surfaceOfLP,
				double surfaceOfOS, double factor, double priceOfM2) {
		super(noOfRealEstate,surnameNameOfOwner,builtYear);
		this.surfaceOfLP = surfaceOfLP;
		this.surfaceOfOS = surfaceOfOS;
		this.factor = factor;
		this.priceOfM2 = priceOfM2;
	}
	
	
	public double value(int year) {
		double factorOfOld = 1-(year-builtYear)*0.01;
		factorOfOld = factorOfOld>0.5 ? factorOfOld:0.5;
		return (surfaceOfLP+surfaceOfOS/2)*priceOfM2*factor*factorOfOld;
	}
	
	
	public void printAll() {
		super.printAll();
		System.out.println("Surface of living space: "+surfaceOfLP+" m2");
		System.out.println("Surface of other spaces: "+surfaceOfOS+" m2");
		System.out.println("Factor: "+factor);
		System.out.println("Price of m2 of the flat: "+priceOfM2+" EUR");
	}

}
