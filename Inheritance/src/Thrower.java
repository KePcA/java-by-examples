
public class Thrower extends Athlete {
	
	private String nameOfDiscipline;
	private double[] lenghts;
	
	
	public Thrower(int startNo, String surname, String name, String club, String nameOfDiscipline) {
		super(startNo,surname,name,club);
		this.nameOfDiscipline = nameOfDiscipline;
		lenghts = new double[6];
	}
	
	
	public void enterResult() {
		System.out.println("Entering lengths:");
		for(int i=0; i<lenghts.length; i++) {
			System.out.print((i+1)+". throw: ");
			lenghts[i] = ReadData.readDouble();
		}
	}
	
	
	public String toString() {
		String s = super.toString();
		s += "Name of the discipline: "+nameOfDiscipline+"\n";
		s += "Lengths: \n";
		for(int i=0; i<lenghts.length; i++) {
			s += lenghts[i]+" m ";
		}
		return s+"\n";
	}
	
	

}
