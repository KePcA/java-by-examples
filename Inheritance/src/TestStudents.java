
public class TestStudents {
	
	
	public static void main(String[] args) {
		
		Student s = new Student(63070012,"Berlec","Mateja",1,10);
		s.printAll();
		System.out.println();
		System.out.println("No. of passed exams: "+s.getNoOfPassedExams());
		System.out.println("Can advance: "+(s.canAdvance(6)?"DA":"NE"));
		System.out.println();
		System.out.println("First entring of grades...");
		s.insertGrade(0,7);
		s.insertGrade(2,9);
		s.insertGrade(3,5);
		s.insertGrade(4,10);
		s.insertGrade(7,9);
		s.insertGrade(9,8);
		System.out.println();
		s.printAll();
		System.out.println("No. of passed exams: "+s.getNoOfPassedExams());
		System.out.println("Can advance: "+(s.canAdvance(6)?"DA":"NE"));
		System.out.println();
		System.out.println("First entring of grades...");
		s.insertGrade(3,9);
		s.insertGrade(5,10);
		s.printAll();
		System.out.println("No. of passed exams: "+s.getNoOfPassedExams());
		System.out.println("Can advance: "+(s.canAdvance(6)?"DA":"NE"));
		System.out.println();
		PartTimeStudent pts = new PartTimeStudent(63050245,"Frenk","Andrej",3,10,false);
		pts.printAll();
		System.out.println();
		System.out.println("Entring of grades: ");
		pts.insertGrade(0,10);
		pts.insertGrade(1,9);
		pts.insertGrade(3,10);
		pts.insertGrade(4,10);
		pts.insertGrade(5,8);
		pts.insertGrade(6,9);
		pts.insertGrade(7,10);
		pts.insertGrade(9,10);
		System.out.println();
		pts.printAll();
		System.out.println();
		System.out.println("No. of passed exams: "+pts.getNoOfPassedExams());
		System.out.println("Can advance: "+(pts.canAdvance(6)?"DA":"NE"));
		System.out.println();
		System.out.println("Fee payment ...");
		pts.setPayedStudyFee(true);
		System.out.println();
		pts.printAll();
		System.out.println();
		System.out.println("Can advance: "+(pts.canAdvance(6)?"DA":"NE"));
		
	}
	

}
