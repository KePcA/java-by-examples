
public class Bus extends Vehicle {
	
	private int noOfSeats;
	
	public Bus(String regNo, String manufacturer, String model, int noOfSeats) {
		super(regNo,manufacturer,model);
		this.noOfSeats = noOfSeats;
	}
	
	
	public void printData() {
		super.printData();
		System.out.println("No. of seats: "+noOfSeats);
	}

}
