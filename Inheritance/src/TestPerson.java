
public class TestPerson {
	
	
	public static void main(String[] args) {
		Person p = new Person(23412340,"Novak","Janez",1950);
		p.printAll();
		System.out.println();
		TaxPayer tp = new TaxPayer(76534233,"Oblak","Ana",1962,14563.55);
		tp.printAll();
		System.out.printf("Tax: %.2f EUR %n",tp.calculateTax());
	}

}
