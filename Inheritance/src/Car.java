
public class Car extends Vehicle {
	
	private int highestSpeed;
	private double acceleration;
	
	
	public Car(String regNo, String manufacturer, String model, int highestSpeed, double acceleration) {
		super(regNo,manufacturer,model);
		this.highestSpeed = highestSpeed;
		this.acceleration = acceleration;
	}
	
	
	public void printData() {
		super.printData();
		System.out.println("Highest speed: "+highestSpeed+" km/h");
		System.out.println("Acceleration from 0 t0 100 km/h: "+acceleration+" seconds");
	}

	
	public boolean better(Car c) {
		return this.highestSpeed > c.highestSpeed;
	}
}
