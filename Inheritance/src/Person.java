
public class Person {
	
	private int taxNumber;
	private String surname;
	private String name;
	private int yearOfBirth;
	
	public Person(int taxNumber, String surname, String name, int yearOfBirth) {
		this.taxNumber = taxNumber;
		this.surname = surname;
		this.name = name;
		this.yearOfBirth = yearOfBirth;
	}
	
	public void printAll() {
		System.out.println("Tax number: "+taxNumber);
		System.out.println("Person: "+surname+" "+name);
		System.out.println("Year of birth: "+yearOfBirth);
	}

}
