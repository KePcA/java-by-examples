
public class Runner extends Athlete {
	
	private int lenght;
	private byte hours, minutes, seconds, miliSeconds;
	
	
	public Runner(int startNo, String surname, String name, String club, int lenght) {
		super(startNo,surname,name,club);
		this.lenght = lenght;
	}
	
	
	public void enterResult() {
		System.out.println("Time of the run:");
		do {
			System.out.print("Hours:");
			hours = (byte)ReadData.readInt();
		} while(hours<0 || hours>5);
		
		do {
			System.out.print("Minutes:");
			minutes = (byte)ReadData.readInt();
		} while(hours<0 || hours>59);
		
		do {
			System.out.print("Seconds:");
			seconds = (byte)ReadData.readInt();
		} while(hours<0 || hours>59);
		
		do {
			System.out.print("Miliseconds:");
			miliSeconds = (byte)ReadData.readInt();
		} while(hours<0 || hours>99);
		System.out.println();
	}
	
	
	public String toString() {
		String s = super.toString();
		s += "Lenght: "+lenght+" m \n";
		s += "Time of the run: "+hours+" hours, "+minutes+" minutes, "+seconds+" seconds, "+miliSeconds+" miliSeconds \n";
		return s;
	}

}
