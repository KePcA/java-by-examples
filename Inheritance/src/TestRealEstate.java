
public class TestRealEstate {
	
	
	public static void main(String[] args) {
		
		Flat f = new Flat(3421,"Novak Andrej",1985,54.5,8.5,1.2,2450.0);
		f.printAll();
		System.out.println("Value of the flat: "+f.value(2008)+" EUR");
		System.out.println();
		
		House h = new House(2541,"Majer Petra",1932,185.0,25.0,1.8,1900.0,450.0,145.0);
		h.printAll();
		System.out.println("Value of the house: "+h.value(2008)+" EUR");
		System.out.println();		
	}

}
