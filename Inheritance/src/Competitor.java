
public abstract class Competitor {
	
	
	private int startNo;
	
	public Competitor(int startNo) {
		this.startNo = startNo;
	}
	
	public abstract void enterResult();
	public abstract double returnResult();
	
}
