
public class House extends Flat {
	
	private double surfaceOfLand;
	private double priceOfM2Land;
	
	public House(int noOfRealEstate, String surnameNameOfOwner, int builtYear, double surfaceOfLP,
			double surfaceOfOS, double factor, double priceOfM2, double surfaceOfLand, double priceOfM2Land) {
		super(noOfRealEstate,surnameNameOfOwner,builtYear,surfaceOfLP,surfaceOfOS,factor,priceOfM2);
		this.surfaceOfLand = surfaceOfLand;
		this.priceOfM2Land = priceOfM2Land;	
	}
	
	
	public double value(int year) {
		return super.value(year) + surfaceOfLand*priceOfM2Land;
	}
	
	
	public void printAll() {
		super.printAll();
		System.out.println("Surface of land: "+surfaceOfLand+" m2");
		System.out.println("Price of m2 of land: "+priceOfM2Land+" EUR");
	}

}
