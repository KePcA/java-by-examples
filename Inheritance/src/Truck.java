
public class Truck extends Vehicle {
	
	private int power;
	private int loadCapacity;
	
	public Truck(String regNo, String manufacturer, String model, int power, int loadCapacity) {
		super(regNo,manufacturer,model);
		this.power = power;
		this.loadCapacity = loadCapacity;
	}
	
	
	public void printData() {
		super.printData();
		System.out.println("Power: "+power+" kW");
		System.out.println("Load capacity: "+loadCapacity+" tons");
	}

}
