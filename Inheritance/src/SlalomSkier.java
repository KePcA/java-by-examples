
public class SlalomSkier extends Competitor {
	
	private double timeOfFirstSki;
	private double timeOfSecondSki;
	
	
	public SlalomSkier(int startNo) {
		super(startNo);
	}
	
	public SlalomSkier(int startNo, double timeOfFirstSki, double timeOfSecondSki) {
		super(startNo);
		this.timeOfFirstSki = timeOfFirstSki;
		this.timeOfSecondSki = timeOfSecondSki;
	}
	
	
	public void enterResult() {
		System.out.print("Time of first ski: ");
		timeOfFirstSki = ReadData.readDouble();
		System.out.print("Time of second ski: ");
		timeOfSecondSki = ReadData.readDouble();
	}
	
	public double returnResult() {
		return timeOfFirstSki + timeOfSecondSki;
	}
}
