
public class PartTimeStudent extends Student {
	
	private boolean payedStudyFee;
	
	
	public PartTimeStudent(int regNo, String surname, String name, int year, int noOfExams, boolean payedStudyFee) {
		super(regNo,surname,name,year,noOfExams);
		this.payedStudyFee = payedStudyFee;
	}
	
	public void setPayedStudyFee(boolean payedStudyFee) {
		this.payedStudyFee = payedStudyFee;
	}
	
	
	public boolean canAdvance(int noOfExamsToAdvance) {
		return (super.canAdvance(noOfExamsToAdvance) && payedStudyFee);
		
	}
	
	public void printAll() {
		super.printAll();
		System.out.println("Payed study fee: "+(payedStudyFee?"DA":"NE"));
	}

}
