
public class TestSki {
	
	
	
	
	public static void main(String[] args) {
		
		final int NO_COMPETITORS = 10;
		Competitor[] c = new Competitor[NO_COMPETITORS];
		
		for(int i=0; i<c.length; i++) {
			System.out.println("Pick type of competitior:");
			System.out.print("(1-Skier, 2-Slalom skier, 3-Comb skier): ");
			int response = ReadData.readInt();
			System.out.print("Start number: ");
			int startNo = ReadData.readInt();
			switch(response) {
				case 1:
					c[i] = new Skier(startNo);
					break;
				case 2:
					c[i] = new SlalomSkier(startNo);
					break;
				case 3:
					c[i] = new CombSkier(startNo);
			}
			c[i].enterResult();
		}
		
		findBest(c);
	}
	
	
	
	public static void findBest(Competitor[] c) {
		int bestSkier = -1;
		int bestSlalomSkier = -1;
		int bestCombSkier = -1;
		double timeBestSkier = Double.MAX_VALUE;
		double timeBestSlalomSkier = Double.MAX_VALUE;
		double timeBestCombSkier = Double.MAX_VALUE;
		for(int i=0; i<c.length; i++) {
			double time = c[i].returnResult();
			if(c[i] instanceof Skier) {
				if(time<timeBestSkier) {
					bestSkier = i;
					timeBestSkier = time;
				}
			}
			else if(c[i] instanceof SlalomSkier) {
				if(time<timeBestSlalomSkier) {
					bestSlalomSkier = i;
					timeBestSlalomSkier = time;
				}
			}
			else {
				if(time<timeBestCombSkier) {
					bestCombSkier = i;
					timeBestCombSkier = time;
				}
			}
		}
		System.out.println("Best skier");
		System.out.println(" - index: "+bestSkier);
		System.out.println(" - time:  "+timeBestSkier);
		
		System.out.println("Best slalom skier");
		System.out.println(" - index: "+bestSlalomSkier);
		System.out.println(" - time:  "+timeBestSlalomSkier);
		
		System.out.println("Best comb skier");
		System.out.println(" - index: "+bestCombSkier);
		System.out.println(" - time:  "+timeBestCombSkier);
		
	}

}
