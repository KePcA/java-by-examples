
public abstract class RealEstate {
	
	private int noOfRealEstate;
	private String surnameNameOfOwner;
	protected int builtYear;
	
	
	
	public RealEstate(int noOfRealEstate, String surnameNameOfOwner, int builtYear) {
		this.noOfRealEstate = noOfRealEstate;
		this.surnameNameOfOwner = surnameNameOfOwner;
		this.builtYear = builtYear;
	}
	
	
	public abstract double value(int year);
	
	public void printAll() {
		System.out.println("Number of real estate: "+noOfRealEstate);
		System.out.println("Owner: "+surnameNameOfOwner);
		System.out.println("Built year: "+builtYear);
	}

}
