
public class TaxPayer extends Person {
	
	private double taxBase;
	
	
	public TaxPayer(int taxNumber, String surname, String name, int yearOfBirth, double taxBase) {
		super(taxNumber,surname,name,yearOfBirth);
		this.taxBase = taxBase;
	}
	
	
	public double calculateTax() {
		if(taxBase<=6800) {
			return 0.16*taxBase;
		}
		else if(taxBase>6800 && taxBase<=13600) {
			return 1088+0.27*(taxBase-6800);
		}
		else {
			return 2924 + 0.41*(taxBase-13600);
		}
	}
	
	public void printAll() {
		super.printAll();
		System.out.printf("Tax base: %.2f EUR %n",taxBase);
	}

}
