
public class Coding {
	
	
	public static void main(String[] args) {
		
		System.out.print("Enter string: ");
		String string = ReadData.readString();
		while(string.length()!=0) {
			StringBuffer newString = new StringBuffer(string);
			char firstChar = Character.toUpperCase(newString.charAt(0));
			switch(firstChar) {
				case 'N':
					break;
				case '0':
					turnAround(newString);
					break;
				case 'P':
					newString.setCharAt(0,'Z');
					increase(newString);
					break;
				case 'Z':
					newString.setCharAt(0,'P');
					decrease(newString);
					break;
				default:
					System.out.println("Wrong rule");
			}
			System.out.println("After coding "+newString);
			System.out.print("Enter string: ");
			string = ReadData.readString();
		}
		
	}
	
	
	public static void turnAround(StringBuffer string) {
		int l=string.length();
		for(int i=0; i<=l/2; ++i) {
			char current = string.charAt(i);
			string.setCharAt(i,string.charAt(l-i));
			string.setCharAt(l-i,current);
					
		}
	}
	
	
	public static void increase(StringBuffer string) {
		for(int i=0; i<string.length(); i++) {
			char current = string.charAt(i);
			if((current>='A' && current<='Z') || (current>='a' && current<='z') || (current>='0' && current<='9')) {
				switch(current) {
				case 'Z':
					string.setCharAt(i,'A');
					break;
				case 'z':
					string.setCharAt(i,'a');
					break;
				case '9':
					string.setCharAt(i,'0');
					break;
				default:
					string.setCharAt(i,current++);
				}
			}	
		}
	}
	
	
	public static void decrease(StringBuffer string) {
		for(int i=0; i<string.length(); i++) {
			char current = string.charAt(i);
			if((current>='A' && current<='Z') || (current>='a' && current<='z') || (current>='0' && current<='9')) {
				switch(current) {
				case 'A':
					string.setCharAt(i,'Z');
					break;
				case 'a':
					string.setCharAt(i,'z');
					break;
				case '0':
					string.setCharAt(i,'9');
					break;
				default:
					string.setCharAt(i,current--);
				}
			}	
		}
	}
	
	
	
	
	
	

}
