
public class OrderOfWords {
	
	
	public static void main(String[] args) {
		
		String string = ReadData.readString();
		while(string.length()!=0) {
			System.out.println(turnOrderOfWords(string));
			string = ReadData.readString();
		}
	}
	
	
	//Turnes order of the words in a string
	public static StringBuffer turnOrderOfWords(String string) {
		StringBuffer newString = new StringBuffer();
		int beginning = 0;		
		int end = string.indexOf(' ');							 //Index of beginning and the end of the current word
		while(end!=-1) {                                          //While there is still another word
			newString.insert(0,string.substring(beginning,end)); //Insert words to the beginning of the new string
			newString.insert(0,' ');                             //Insert whitespace
			beginning = end+1;                                   //Change indexes for a new word
			end = string.indexOf(' ',beginning);
		}
		newString.insert(0,string.substring(beginning,string.length())); //Insert last word
		return newString;
	}

}
