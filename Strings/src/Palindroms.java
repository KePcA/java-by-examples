import java.util.Scanner;

public class Palindroms {
	
	
	public static void main(String[] args) {
		//Whatever
		Scanner scanner = new Scanner(System.in);
		String s = scanner.nextLine();
		System.out.println(isPalindrom(s));
	}
	
	
	//Returns true if a string is palindrom
	public static boolean isPalindrom(String s) {
		int i = 0;
		int j = s.length()-1;
		while((i<j) && (s.charAt(i)==s.charAt(j))) {
			i++;
			j--;
		}
		return i>=j;
	}

}
