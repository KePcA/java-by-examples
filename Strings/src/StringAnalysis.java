
public class StringAnalysis {
	
	
	public static void main(String[] args) {
		String string = ReadData.readString();
		while(string.length()!=0) {
			System.out.println(noOfUpperCaseChar(string));
			System.out.println(noOfLowerCaseChar(string));
			System.out.println(numberOfWords(string));
			string = ReadData.readString();
		}
	}
	
	//Returns # of upper case characters
	public static int noOfUpperCaseChar(String string) {
		int no = 0;
		for(int i=0; i<string.length(); i++) {
			if(Character.isUpperCase(string.charAt(i))) {
				no++;
			}
		}
		return no;
	}
	
	//Returns # of lower case characters
	public static int noOfLowerCaseChar(String string) {
		int no = 0;
		for(int i=0; i<string.length(); i++) {
			if(Character.isLowerCase(string.charAt(i))) {
				no++;
			}
		}
		return no;
	}
	
	
	//Returns # of words in a string (there can be multiple whitespaces)
	public static int numberOfWords(String string) {
		int no = 0;
		int i = 0;
		while(i<string.length()) {
			//WhiteSpace
			while(i<string.length() && Character.isWhitespace(string.charAt(i))) { i++; }
			if(i==string.length()) { return no; }
			while(i<string.length() && Character.isLetterOrDigit(string.charAt(i))) { i++; }
			no++;
		}
		return no;
	}
	
	

}
