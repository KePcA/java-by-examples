
public class TurnWordsAround {
	
	
	public static void main(String[] args) {
		
		String string = ReadData.readString();
		while(string.length()!=0) {
			System.out.println(turnWordsAround(string));
			System.out.println(turnWordsAround2(string));
			System.out.println();
			string = ReadData.readString();
		}
	}
	
	
	//Returns new string with words being each turned around
	public static StringBuffer turnWordsAround(String string) {
		StringBuffer newString = new StringBuffer();
		int beginning = 0;											//Index of the beginning of the current word
		int i=0;													//Initialize index
		while(i<string.length()) {
			if(string.charAt(i) != ' ') {				
				newString.insert(beginning,string.charAt(i));		//If its not a whitespace insert current char to the 
				i++;												//beginning of the new string
			}
			else {
				newString.append(' ');								//Place a whitespace
				while(string.charAt(i) == ' ') { i++; }				//Ignore all the whitespaces in a row
				beginning = newString.length();						//Place index to the beginning of the new word
			}
		}
		return newString;
	}
	
	//Returns new string with words being each turned around using split method
	public static StringBuffer turnWordsAround2(String string) {
		StringBuffer newString = new StringBuffer();
		String[] words = string.split(" +");				//Splits word by one or more whitespacec
		int i=0;
		while(i<words.length-1) {
			StringBuffer nw = new StringBuffer(words[i]);	
			newString.append(nw.reverse());					//Append revered words to a new string
			newString.append(' ');							//Append whitespace
			i++;
		}
		StringBuffer nw = new StringBuffer(words[i]);		//Add last word - it is without the whitespace at the end
		newString.append(nw.reverse());
		return newString;
	}

}
