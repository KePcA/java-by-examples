
public class Anagrams {
	
	
	
	public static void main(String[] args) {
		
		String s1 = "afrtghz";
		String s2 = "rtfazgh";
		String s3 = "whatever";
		
		System.out.println("Sorting: "+isAnagramSort(s1,s3));
		System.out.println("Removing: "+isAnagramRemove(s1,s3));
		
	}
	
	
	//Check if two strings are anagrams by sorting
	public static boolean isAnagramSort(String s1, String s2) {
		if(s1.length()==s2.length()) {
			StringBuffer n1 = new StringBuffer(s1);
			StringBuffer n2 = new StringBuffer(s1);
			sort(n1);
			sort(n2);
			return(n1.toString().equals(n2.toString()));
		}
		return false;
	}
	
	//Sort the string by the characters
	public static void sort(StringBuffer s) {
		for(int i=0; i<=s.length()-2; i++) {
			int iMin = i;
			char vMin = s.charAt(i);
			for(int j=i+1; j<=s.length()-1; j++) {
				if(s.charAt(j) < vMin) {
					vMin = s.charAt(j);
					iMin = j;
				}
			}
			s.setCharAt(iMin, s.charAt(i));
			s.setCharAt(i,vMin);
		}
	}
	
	//Check if two string are anagrams by removing the characters
	public static boolean isAnagramRemove(String s1, String s2) {
		if(s1.length()==s2.length()) {
			for(int i=0; i<s1.length(); i++) {
				char currentChar = s1.charAt(i);
				int index = s2.indexOf(currentChar);
				if(index!=-1) {
					s2 = s2.substring(0,index) + s2.substring(index+1,s2.length());
				}
				else {
					return false;
				}
			}
			return s2.length()==0;
		}
		return false;
	}

}
