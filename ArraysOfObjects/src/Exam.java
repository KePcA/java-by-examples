
public class Exam {
	
	private String courseName;
	private int grade;
	
	public Exam(String courseName, int grade) {
		this.courseName = courseName;
		this.grade = grade;
	}
	
	public int getGrade() {
		return grade;
	}

}
