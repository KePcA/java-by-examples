
public class BallThrowers {
	
	
	static final int NO_THROWERS = 12;
	static Thrower[] results = new Thrower[NO_THROWERS];
	
	
	public static void main(String[] args) {
		
		readData();
		printAverageDistances();
		
	}
	
	
	public static void readData() {
		for(int i=0; i<NO_THROWERS; i++) {
			System.out.println((i+1)+" THROWER: ");
			System.out.print("Start number: ");
			int startNo = ReadData.readInt();
			System.out.print("Surname: ");
			String surname = ReadData.readString();
			System.out.print("Name: ");
			String name = ReadData.readString();
			double[] distances = new double[6];
			for(int j=0; j<6; j++) {
				System.out.print((j+1)+". throw: ");
				distances[j] = ReadData.readDouble();
			}
			results[i] = new Thrower(startNo,surname,name,distances);
		}
	}
	
	
	
	//Print average distances for all the throwers in the array
	public static void printAverageDistances() {
		for(Thrower t: results) {
			System.out.print(t.getStartNo()+" "+t.getSurname()+" "+t.getName());
			System.out.println(" "+averageDistance(t));
		}
	}
	
	//Returns average distance for the given thrower
	public static double averageDistance(Thrower t) {
		double[] distances = t.returnDistances();
		double sum = 0;
		int noOfThrows = 0;
		for(double distance: distances) {
			if(distance!=0.0) {
				noOfThrows++;
				sum += distance;
			}
		}
		if(noOfThrows>0) {
			return sum/noOfThrows;
		}
		else {
			return 0.0;
		}
	}

}
