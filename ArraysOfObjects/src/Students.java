
public class Students {
	
	public static final int NO_STUDENTS = 5;
	private static Student[] students = new Student[NO_STUDENTS];
	
	public static void main() {
		readData();
		System.out.println(bestStudent());
	}
	
	
	
	public static void readData() {
		
	}
	
	//Finds a best student - the one with the highest grade or if there are more students with the same grade
	//then the one who has more completed exams.
	public static int bestStudent() {
		int indexBest = 0;
		double bestAverageGrade = students[0].getAverageGrade();
		for(int i=1; i<students.length; i++) {
			double currentAverageGrade = students[i].getAverageGrade();
			if( (currentAverageGrade > bestAverageGrade) || 
				((currentAverageGrade==bestAverageGrade) && (students[i].getNoOfGrades()>students[indexBest].getNoOfGrades()))) {
				bestAverageGrade = currentAverageGrade;
				indexBest = i;
			}
		}		
		return indexBest;
	}

}
