
public class Thrower {
	
	private int startNo;
	private String surname;
	private String name;
	private double[] distances;
	
	
	public Thrower(int startNo, String surname, String name, double[] distances) {
		this.startNo = startNo;
		this.surname = surname;
		this.name = name;
		this.distances = distances.clone();
	}
	
	public int getStartNo() {
		return startNo;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public String getName() {
		return name;
	}
	
	public double[] returnDistances() {
		return distances.clone();
	}

}
