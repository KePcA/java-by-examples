
public class Team {
	
	private String name;
	private int noOfWins, noOfTies, noOfLosses, noOfGivenScores, noOfReceivedScores;
	
	public Team(String name) {
		this.name = name;
		noOfWins = 0;
		noOfTies = 0;
		noOfLosses = 0;
		noOfGivenScores = 0;
		noOfReceivedScores = 0;
	}
	
	//Enters a result after a game and adds a win, loss or a tie
	public void enterResult(int givenScores, int receivedScores) {
		if(givenScores > receivedScores) {
			noOfWins++;
		}
		else if(givenScores < receivedScores) {
			noOfLosses++;
		}
		else {
			noOfTies++;
		}
		this.noOfGivenScores += givenScores;
		this.noOfReceivedScores += receivedScores;
		
	}
	
	public String getName() {
		return name;
	}
	
	public int getNoOfWins() {
		return noOfWins;
	}
	
	public int getNoOfTies() {
		return noOfTies;
	}
	
	public int getNoOfLosses() {
		return noOfLosses;
	}
	
	public int getNoOfGivenScores() {
		return noOfGivenScores;
	}
	
	public int getNoOfReceivedScores() {
		return noOfReceivedScores;
	}
	
	
	public int getNoOfGames() {
		return noOfWins + noOfTies + noOfLosses;
	}
	
	public int getPoints() {
		return noOfWins*3 + noOfTies;
	}
	
	public int scoreDifference() {
		return noOfGivenScores - noOfReceivedScores;
	}
	
	public String toString() {
		return name+" "+getNoOfGames()+" "+noOfWins+" "+noOfTies+" "
				+noOfLosses+" "+noOfGivenScores+" "+noOfReceivedScores+" "+getPoints();
	}
	
	public boolean better(Team t) {
		if(this.getPoints() > t.getPoints()) {
			return true;
		}
		else if(this.getPoints()==t.getPoints()) {
			if(this.scoreDifference() > t.scoreDifference()) {
				return true;
			}
			else if(this.scoreDifference()==t.scoreDifference()) {
				return this.getNoOfGivenScores()>t.getNoOfGivenScores();
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

}
