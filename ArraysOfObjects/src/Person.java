
public class Person {

	
	private String surname;
	private String name;
	private Date dateOfBirth;
	
	
	public Person(String surname, String name, Date dateOfBirth) {
		this.surname = surname;
		this.name = name;
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public String getName() {
		return name;
	}
	
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	
}
