
public class Student {
	
	private int idNumber;
	private String surname;
	private String name;
	private Exam[] exams;
	
	
	public Student(int idNumber, String surname, String name, Exam[] exams) {
		this.idNumber = idNumber;
		this.surname = surname;
		this.name = name;
		this.exams = exams.clone();
	}
	
	public int getIdNumber() {
		return idNumber;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public String getName() {
		return name;
	}
	
	public int getNoOfGrades() {
		return exams.length;
	}
	
	public Exam[] getExams() {
		return exams.clone();
	}
	
	//Calculates average grade of all of the exams of this current student
	public double getAverageGrade() {
		int noOfExams = getNoOfGrades();
		if(noOfExams == 0) {
			return 0.0;
		}
		else {
			int sum = 0;
			for(int j=0; j<noOfExams; j++) {
				sum += exams[j].getGrade();
			}
			return (double)sum/noOfExams;
		}
	}
}
