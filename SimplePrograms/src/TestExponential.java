
public class TestExponential {
	
	
	public static void main(String[] args) {
		
		System.out.print("Precision: ");
		double newEps = ReadData.readDouble();
		Exponential.setEps(newEps);
		System.out.print("Lower bound: ");
		double lowerBound = ReadData.readDouble();
		System.out.print("Upper Bound: ");
		double upperBound = ReadData.readDouble();
		double x = lowerBound;
		while (x<=upperBound) {
			System.out.printf("%5.1f",x);
			System.out.printf("%17.7f",Exponential.exp(x));
			System.out.printf("%17.7f",Exponential.exp1(x));
			System.out.printf("%17.7f",Math.exp(x));
			System.out.println();
			x+=0.1;
		}
		
	}

}
