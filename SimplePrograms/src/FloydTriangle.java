
public class FloydTriangle {
	
	
	public static void main(String[] args) {
		
		System.out.print("Insert # of rows: ");
		int numberOfRows = ReadData.readInt();	
		int number = 0;
		
		for(int row=1; row<=numberOfRows; row++) {
			for(int col=1; col<= row; col++) {
				number++;
				System.out.printf("%4d",number);
			}
			System.out.println();
		}
	}
	
	
	

}
