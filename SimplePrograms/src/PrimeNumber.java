
public class PrimeNumber {

	
	public static void main(String[] args) {
		
		System.out.print("Enter number (0=end)): ");
		int number = ReadData.readInt();
		
		while(number!=0) {
			if(isPrimeOpt(number)) {
				System.out.println(number+" is a prime number.");
			}
			else {
				System.out.println(number+" is not a prime number.");
			}
			System.out.print("Enter number (0=end)): ");
			number = ReadData.readInt();
			
		}
	}
	
	
	//Naive method to check if number is a prime
	public static boolean isPrime(int number) {
		for(int i=2; i<=number/2; i++) {
			if(number%i == 0) { 
				return false; 
			}
		}
		return true;
	}
	
	public static boolean isPrimeOpt(int number) {
		if(number <= 3) {
			return false;
		}
		else if(number%2 != 0) {
			int upperBound = (int)Math.sqrt(number);
			for(int d=3; d<=upperBound; d+=2) {
				if(number%d == 0) {
					return false;
				}
			}
			return true;
		}
		else {
			return false;
		}
	}
	
	
}
