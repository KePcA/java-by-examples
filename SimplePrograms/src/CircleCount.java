
public class CircleCount {
	
	
	public static void main(String[] args) {
		
		final int A = 10;
		final int N = 8;
		
		int in = 0;
		int out = 0;
		int cross = 0;
		
		for(int i=0; i<N; i++) {
			System.out.print("Insert diameter: ");
			double diameter = ReadData.readDouble();
			if(diameter < A) {
				in++;
			}
			else if (diameter > A*Math.sqrt(2)) {
				out++;
			}
			else {
				cross++;
			}
		}
		
		System.out.println("Inside: "+in);
		System.out.println("Outside: "+out);
		System.out.println("Cross: "+cross);
		
	}
	
	
	
	

}
