
public class DiceThrow {
	
	
	public static void main(String[] args) {
		
		System.out.print("Number of throws: ");
		int n = ReadData.readInt();
		
		int st1=0;
		int st2=0;
		int st3=0;
		int st4=0;
		int st5=0;
		int st6=0;
		
		for(int i=1; i<=n; i++) {
			int diceThrow = (int)(Math.random()*6) + 1;
			switch (diceThrow) {
				case 1: st1++; break;
				case 2: st2++; break;
				case 3: st3++; break;
				case 4: st4++; break;
				case 5: st5++; break;
				case 6: st6++;
			
			}
		}
		System.out.println(st1+" "+st2+" "+st3+" "+st4+" "+st5+" "+st6);
		
	}

}
