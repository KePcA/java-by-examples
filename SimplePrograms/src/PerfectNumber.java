
public class PerfectNumber {
	
	
	public static void main(String[] args) {
		
		System.out.print("Enter nMax: ");
		int nMax = ReadData.readInt();
		
		for(int n=1; n<=nMax; n++) {
			if(isPerfectOpt(n)) {
				System.out.println(n);
			}
		}
		
		
	}
	
	//Naive method for perfect number
	public static boolean isPerfect(int n) {
		int sum = 0;
		for(int d=1; d<=n/2; d++) {
			if(n%d == 0) {
				sum += d;
			}
		}
		return sum==n;
	}
	
	
	//Optimised method for perfect number
	public static boolean isPerfectOpt(int n) {
		int sum = 0;
		int upperBound = (int)Math.sqrt(n);
		for(int d=1; d<=upperBound; d++) {
			if(n%d == 0) {
				sum+=d;
				sum+=n/d;
			}
		}
		sum-=n;
		if (upperBound*upperBound == n) {
			sum-=upperBound;
		}
		return sum==n;
	}
	
	
}
