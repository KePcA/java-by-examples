
public class BishopMove {
	
	
	
	public static void main(String[] args) {
		
		System.out.print("Starting row: ");
		int startRow = ReadData.readInt();
		
		System.out.print("Starting column: ");
		int startCol = ReadData.readInt();
		
		System.out.println();
		
		for(int row=1; row<=8; row++) {
			for(int col=1; col<=8; col++) {
				if( (row-col == startRow-startCol) || (row+col == startRow+startCol)) {
					System.out.print("*");
				}
				else if ((row+col)%2 == 0) {
					System.out.print("W");
				}
				else {
					System.out.print("B");
				}
			}
			System.out.println();
		}
	}
	
	
	

}
