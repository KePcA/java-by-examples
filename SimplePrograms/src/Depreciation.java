
public class Depreciation {
	
	
	
	public static void main (String[] args) {
		boolean finish = false;
		int response = 0;
		int numberOfYears = 5;
		double value = 0;
		while(!finish) {
			System.out.print("Pick method(1-3) or end(4): ");
			response = ReadData.readInt();
			if(response>=1 && response<=3) {
				System.out.print("Value: ");
				value = ReadData.readDouble();
				System.out.print("Number of years: ");
				numberOfYears = ReadData.readInt();
				
				switch(response) {
					case 1:
						method1(value, numberOfYears);
						break;
					case 2:
						method2(value, numberOfYears);
						break;
					case 3:
						method3(value, numberOfYears);
						break;
					case 4:
						System.out.println("End");
						finish = true;
						break;
					default:
						System.out.println("Wrong answer");
				}
			}
		}
	}
	
	
	public static void method1(double value, int years) {
		printHead();
		double lwv = value/years;
		for(int i=1; i<=years; i++) {
			value -= lwv;
			System.out.printf("%2d%10.0f%10.0f%n",i,lwv,value);
		}
		
	}
	

	
	public static void method2(double value, int years) {
		double lwv = 0;
		for(int i=1; i<=years; i++) {
			lwv = 2*value/years;
			value -= lwv;
			System.out.printf("%2d%10.0f%10.0f%n",i,lwv,value);
		}
	}
	
	
	
	public static void method3(double value, int years) {
		double lwv = 0;
		double iniValue = value;
		for(int i=1; i<=years; i++) {
			lwv = (years-i+1)*iniValue/(years*(years+1)/2);
			value = value - lwv;
			System.out.printf("%2d%10.0f%10.0f%n",i,lwv,value);
		}
		
	}
	
	public static void printHead() {
		System.out.println("end      decreased      current");
		System.out.println("year      value           value");
		System.out.println("-------------------------------");
	}

}
