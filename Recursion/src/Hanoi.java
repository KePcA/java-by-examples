
public class Hanoi {
	
	
	static final int N=3;
	static int[][] columns = new int[N][3];
	static int[] height = new int[3];
	
	public static void main(String[] args) {
		
		for(int i=0; i<N; i++) {
			columns[i][0] = N-1;
		}
		for(int i=0; i<N; i++) {
			for(int j=0; j<=2; j++) {
				columns[i][j] = 0;
			}
 		}
		
		
		height[0] = N-1;
		height[1] = -1;
		height[2] = -1;
		
		
		print();
		
		game(0,1,N);
	}
	
	
	
	public static void game(int from, int to, int noOfDiscs) {
		if(noOfDiscs==1) {
			moveDisc(from,to);
		}
		else {
			int aux = 3-from-to;
			game(from,aux,noOfDiscs-1);
			moveDisc(from,to);
			game(aux,to,noOfDiscs-1);
		}
	}
	
	public static void moveDisc(int from, int to) {
		height[to]++;
		columns[height[to]][to] = columns[height[from]][from];
		columns[height[from]][from] = 0;
		height[from]--;
		print();
	}
	
	private static void print() {
		for(int i=N-1; i>=0; i--) {
			for(int j=0; j<=2; j++) {
				if(columns[i][j] != 0) {
					System.out.printf("%5d",columns[i][j]);
				}
				else {
					System.out.print("     ");
				}
			}
			System.out.println();
		}
		System.out.println("-----------------------");
		System.out.println();
	}

}
