
public class AnagramsRec {
	
	
	public static void main(String[] args) {
		
		System.out.println("First string: ");
		String string1 = ReadData.readString();
		System.out.println("Second string: ");
		String string2 = ReadData.readString();
		System.out.println(areAnagramsRec(string1,string2));
	}
	
	
	public static boolean areAnagramsRec(String string1, String string2) {
		if(string1.equals(string2)) {
			return true;
		}
		else {
			int position = string2.indexOf(string1.charAt(0));
			if(position==-1) {
				return false;
			}
			else {
				string1 = string1.substring(1,string1.length());
				string2 = string2.substring(0,position) + string2.substring(position+1,string2.length());
				return areAnagramsRec(string1,string2);
			}
		}
	}

}
