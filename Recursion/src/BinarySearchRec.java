
public class BinarySearchRec {
	
	
	public static void main(String[] args) {
		
		int[] t = {1,4,5,7,8,9,10,15,21,23};
		int x, poz;
		System.out.print("Enter the number: ");
		x = ReadData.readInt();
		while(x!=0) {
			poz = searchRec(t,x,0,t.length);
			if(poz!=-1) {
				System.out.print("Number "+x+" is at position "+poz);
			}
			else {
				System.out.print("Number "+x+" is not in the array");
			}
			x = ReadData.readInt();
		}
	}
	
	
	
	public static int searchRec(int[] t, int x, int left, int right) {
		//Left side bigger than right -> there is no such x in the array
		if(left>right) {
			return -1;
		}
		//Calculate midd index of the array
		int midd = (left+right)/2;
		//We found it. It's in the middle :)
		if(x==t[midd]) {
			return midd;
		}
		//It's smaller then the midd element -> search in the left side of the array
		else if(x<t[midd]) {
			return searchRec(t,x,left,midd-1);
		}
		//It's bigger then midd element -> search in the right side of the array
		else {
			return searchRec(t,x,midd+1,right);
		}
	}

}
