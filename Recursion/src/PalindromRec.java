
public class PalindromRec {
	
	
	
	public static void main(String[] args) {
		
		System.out.print("Enter string: ");
		String string = ReadData.readString();
		if(isPalindromRec(string)) {
			System.out.println(true);
		}
		else {
			System.out.println(false);
		}
	}
	
	
	//Recursive method for palindrom
	public static boolean isPalindromRec(String string) {
		//Strings of lenght 0 or 1 are palindroms
		int l = string.length();
		if(l<=1) {
			return true;
		}
		//It's palindrom if 1 and last character are the same and the rest of the string is palindrom 
		else if(string.charAt(0) == string.charAt(l-1)) {
			return isPalindromRec(string.substring(1,l-1));
		}
		else {
			return false;
		}
	}

}
