
public class ScienceConference {
	
	
	private static final int NO_COUNTRIES = 25;
	private static final int NO_TYPES = 3;
	private static final int NO_AREAS = 5;
	private static final int[][][] contributions = new int[NO_COUNTRIES][NO_TYPES][NO_AREAS];
	
	
	//All contributions 
	public static void allDistributions() {
		int sum = 0;
		for(int co=0; co<NO_COUNTRIES; co++) {
			for(int typ=0; typ<NO_TYPES; typ++) {             //Move through the array and count all of them
				for(int area=0; area<NO_AREAS; area++) {
					sum += contributions[co][typ][area];
				}
			}
		}
		System.out.println("Number of distributions together: "+sum);
		System.out.println();
				
		
	}
	
	
	//Contributions of a each type
	public static void distributionOfTypes() {
		for(int type=0; type<NO_TYPES; type++) {
			int sum = 0;								//For each type count all distributions
			for(int co=0; co<NO_COUNTRIES; co++) {
				for(int area=0; area<NO_AREAS; area++) {
					sum += contributions[co][type][area];
				}
			}
			System.out.println("Number of distributions of type"+(type+1)+". is "+sum);
		}
		System.out.println();
	}
	
	
	//Max contributions of a given area
	public static void areaWithMostContributions() {
		int maxSum = 0;
		int index = 0;
		for(int area=0; area<NO_AREAS; area++) {
			int sum = 0;
			for(int co=0; co<NO_COUNTRIES; co++) {
				for(int type=0; type<NO_TYPES; type++) {
					sum += contributions[co][type][area];
				}
			}
			if(sum > maxSum) {
				maxSum = sum;
				index = area;
			}
		}
		System.out.println("Area with most contributions ("+(maxSum)+") has index "+index+".");
	}
	
	public static void fillArray() {
		
	}
	

}
