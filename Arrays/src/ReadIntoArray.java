
public class ReadIntoArray {
	
	
	
	public static void main(String[] args) {
		
		
		final int N = 10;
		int[] table = new int[N];
		
		
		for(int i=0; i<table.length; i++) {
			//Read data
			System.out.print("Insert number: ");
			int number = ReadData.readInt();
			
			//Find index at which we insert the number
			int j=0;
			while((j<i) && number>table[j]) {
				j++;
			}
			
			//Move elements bigger than number one place to the right 
			for(int k=i-1; k>=j; k--) {
				table[k+1] = table[k];
			}
			
			//Insert number
			table[j] = number;
		}
			
		for(int i=0; i<table.length; i++) {
				System.out.print(" "+table[i]);
		}
			
			
			
		
	}

}
