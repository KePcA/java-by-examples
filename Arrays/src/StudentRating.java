
public class StudentRating {
	
	
	public static byte[] exam = new byte[10];
	public static byte[] exercise = new byte[10];
	
	private static double[] avgGr = new double[2];
	private static int[] maxGr = new int[3];
	private static double[] std = new double[2];
	
	
	public static void main(String[] args) {
		
		fillData();
		analysis();
		printResults();
		
	}
	
	//Fill random data into the table exam and exercise
	public static void fillData() {
		for(int i=0; i<exam.length; i++) {
			byte examGrade = (byte)(Math.random()*11);
			exam[i] = examGrade;
			if(examGrade<6) {
				exercise[i] = 0;
			}
			else {
				byte exerciseGrade = (byte)(Math.random()*5+6);
				exercise[i] = exerciseGrade;
			}
			System.out.println("["+i+"] "+exam[i]+" "+exercise[i]);
		}
		
	}
	
	//Makes an analysis
	public static void analysis() {
		//count # od positive grades
		int numberOfPos = 0;
		for(int i=0; i<exam.length; i++) {
			//calculate average
			if(exam[i] > 5) {
				numberOfPos++;
				avgGr[0] += exam[i];
				avgGr[1] += exercise[i];
			}
			//Calculate max grade od exams
			if(exam[i] > exam[maxGr[0]]) {
				maxGr[0] = i;
			}
			//Calculate max grade of exercise
			if(exercise[i] > exercise[maxGr[1]]) {
				maxGr[1] = i;
			}
			//Calculates max grade of both exam and exercise
			if((exam[i] + exercise[i]) > (exam[maxGr[2]] + exercise[maxGr[2]])) {
				maxGr[2] = i;
			}
		}
		//Calculates standard deviation
		if(numberOfPos > 1) {
			for(int i=0; i<exam.length; i++) {
				if(exam[i] > 5) {
					std[0] += (exam[i]-avgGr[0]) * (exam[i] - avgGr[0]);
					std[1] += (exercise[i]-avgGr[1]) * (exercise[i]-avgGr[1]);
				}
				
			}
			std[0] = Math.sqrt(std[0]/(numberOfPos-1));
			std[1] = Math.sqrt(std[1]/(numberOfPos-1));
		}
		
	}
	
	public static void printResults() {
		System.out.println("TO DO");
		
	}
	

}
