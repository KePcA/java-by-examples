
public class GameOfLife {
	
	
	private static final int N = 10;
	private static boolean[][] c = new boolean[N][N];
	
	
	public static void main(String[] args) {
		System.out.print("Number of generations: ");
		int noOfGen = ReadData.readInt();
		initialState();
		printState();
		for(int g=1; g<=noOfGen; g++) {
			newGeneration();
			printState();
		}
	}
	 
	
	
	//Insert alive cels
	private static void initialState() {
		int over;
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				c[i][j] = false;
			}
		}
		//Insert live cells
		do {
			System.out.print("Row: ");
			int i = ReadData.readInt();
			System.out.print("Column: ");
			int j = ReadData.readInt();
			c[i][j] = true;
			System.out.print("End? (1-yes, 0-no): ");
			over = ReadData.readInt();
			
		} while(over==0);
	}
	
	//Prints current state of the game
	private static void printState() {
		System.out.print(" ");
		for(int i=0; i<N; i++) { System.out.print(i); }
		System.out.println();
		for(int i=0; i<N; i++) {
			System.out.print(i);
			for(int j=0; j<N; j++) {
				if(c[i][j]) {
					System.out.print("+");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println(i);
		}
		System.out.print(" ");
		for(int j=0; j<N; j++) { System.out.print(j); }
		System.out.println();
		System.out.println();
	}
	
	
	//Calculates new generation
	public static void newGeneration() {
		boolean[][] c1 = new boolean[N][N];
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				int noN = neighbours(i,j);
				if(c[i][j]) {						//It's alive - will stay alive of neighbours==2 or 3
					c1[i][j] = (noN==2 || noN==3);
				}
				else {
					c1[i][j] = noN==3;              //It's dead - will become alive of neighbours==3
				}
			}
		}
		c = c1;
	}
	
	
	//calculates number of neighbours
	public static int neighbours(int i, int j) {
		int noN = 0;
		int[] indexI = {-1,-1,-1,0,0,1,1,1};		//Indexes of all possible neighbours
		int[] indexJ = {-1,0,1,-1,1,-1,0,1};
		for(int index=0; index<8; index++) {
			int neigI = i+indexI[index];			//Get neighbours with those indexes
			int neigJ = j+indexJ[index];
			if(neigI>=0 && neigI<N && neigJ>=0 && neigJ<N) {   //Check if it is in the life space
				if(c[neigI][neigJ]) {							//Check if it is alive
					noN++;
				}
			}
		}
		return noN;
	}

}
