
public class SortNumbers {
	
	
	public static void main(String[] args) {
		
		int[] table = {23,78,36,12,92,46,15,65};
		sort(table);
		for(int i=0; i<table.length; i++) {
			System.out.print(table[i]+" ");
		}
	}
	
	
	public static void sort(int[] table) {
		//Value and index of the min element
		int iMin, vMin;
		
		for(int i=0; i<table.length-2; ++i) {
			iMin = i;
			vMin = table[i];
			//Find the smallest right of the current element
			for(int j=i+1; j<=table.length-1; ++j) {
				if(table[j] < vMin) {
					iMin = j;
					vMin = table[j];
				}
			}
			//Switch two elements
			table[iMin] = table[i];
			table[i] = vMin;
			
		}
	}

}
