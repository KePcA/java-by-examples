
public class SkiJumps {
	
	
	public static void main(String[] args) {
		
		double rates[][] = {{19.0,17.5,18.5,20.0,18.0},
							{14.5,12.5,14.0,15.0,14.0},
							{19.0,19.0,19.0,19.0,19.0},
							{19.5,18.5,19.0,19.0,19.5},
							{20.0,17.5,18.5,18,0,18.0},
							{20.0,20.0,20.0,20.0,20.0},
							{20.0,17.5,17.5,18.0,17.0}};
		
		bestRated(rates);
							
		
	}
	
	
	public static void bestRated(double[][] rates) {
		boolean no20 = true;
		for(int i=0; i<rates.length; i++) {
			for(int j=0; j<rates[i].length; j++) {
				if(rates[i][j] == 20) {
					System.out.println("Start number: "+(i+1));
					no20 = false;
					break;
				}
			}
		}
		if(no20) {
			System.out.print("There is no grade 20");
		}
	}
	
	public static double[] calcRates(double[][] rates) {
		double[] results = new double[rates.length];
		for(int i=0; i<rates.length; i++) {
			results[i] = 0;
			double maxGrade = 0;
			double minGrade = 20;
			for(int j=0; j<rates[i].length; j++) {
				results[i] += rates[i][j];
				if(rates[i][j] > maxGrade) {
					maxGrade = rates[i][j];
				}
				if(rates[i][j] < minGrade) {
					minGrade = rates[i][j];
				}
			}
			results[i] = results[i]-maxGrade-minGrade;
		}
		return results;
	}
	

}
