
public class SearchInArray {
	
	
	public static void main(String[] args) {
		
		int[] tableSorted = {1,4,5,7,8,9,10,15,21,23};
		int x, poz;
		System.out.print("Insert number: ");
		x = ReadData.readInt();
		while(x!=0) {
			poz = searchSorted(tableSorted,x);
			if(poz != -1) {
				System.out.println("Number "+x+" is at pozition "+poz);
			}
			else {
				System.out.println("Number "+x+" is not in the table");
			}
			System.out.print("Insert number: ");
			x = ReadData.readInt();
		}
		
	}
	
	
	public static int searchUnsorted(int[] table, int x) {
		for(int i=0; i<table.length; i++) {
			if(table[i] == x) {
				return i;
			}
		}
		return -1;
	}
	
	
	public static int searchSorted(int[] table, int x) {
		int left = 0;
		int right = table.length - 1;
		while(left<=right) {
			int middle = (left+right)/2;
			if(table[middle] == x) {
				return middle;
			}
			else if(x < table[middle]) {
				right = middle-1;
			}
			else {
				left = middle + 1;
			}
		}
		return -1;
	}

}
