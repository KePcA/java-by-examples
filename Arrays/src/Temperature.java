
public class Temperature {
	
	
	public static void main(String[] args) {
		
		double[][] t = {{-1,2,10,11,15,18,21,22,18,13,11,2},
						{-2,-2,6,11,14,18,22,23,18,10,11,0},
						{-5,3,8,11,15,18,23,24,18,10,11,0},
						{2,-3,9,11,14,18,24,25,18,13,11,0},
						{1,0,7,11,15,18,25,26,18,10,11,0}};
		
		System.out.print("Insert month: ");
		int m = ReadData.readInt();
		System.out.println(yearlyAverage(t,m));
		
	}
	
	
	//Calculates average temp for the given months
	private static double yearlyAverage(double[][] t, int m) {
		double sum = 0;
		for(int year=0; year<t.length; year++) {
			sum += t[year][m-1];
		}
		return sum/t.length;
	}
	
	//Returnes year where the average temp is highest
	private static double warmestYear(double[][] t) {
		double maxYear = 0;
		double maxTemp = -Double.MAX_VALUE;
		for(int year=0; year<t.length; year++) {
			double avgTemp = 0;
			for(int m=0; m<t[year].length; m++) {
				avgTemp += t[year][m];
			}
			avgTemp /= t[year].length;
			if(avgTemp>maxTemp) {
				maxTemp = avgTemp;
				maxYear = year;
			}
			
		}
		return maxYear+1908;
		
	}

}
