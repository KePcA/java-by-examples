
public class DiceThrowArray {
	
	
	public static void main(String[] args) {
		
		
		int[] throwS = new int[6];
		
		//System.out.print("Number of throws: ");
		int n = ReadData.readInt();
		
		for(int i=0; i<=n; i++) {
			int throW = (int)(Math.random()*6) + 1;
			throwS[throW-1]++;
		}
		
		for(int i=0; i<6; i++) {
			System.out.print(throwS[i]+" ");
		}
		
		
	}

}
