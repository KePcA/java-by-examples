import java.util.HashMap;
import java.util.Scanner;

public class Distance2D {
	
	static final int NO_POINTS = 5;
	static double[] x = new double[NO_POINTS];
	static double[] y = new double[NO_POINTS];
	static double[][] dist = new double[NO_POINTS][NO_POINTS];
	
	
	public static void main(String[] args) {
		
		
		
	}
	
	public static void readCoordinates() {
		for(int i=0; i<NO_POINTS; i++) {
			System.out.print("Coordinate x of "+(i+1)+" point: ");
			x[i] = ReadData.readDouble();	
			System.out.print("Coordinate y of "+(i+1)+" point: ");
			y[i] = ReadData.readDouble();
		}
	}
	
	public static void calculateDistances() {
		double dx, dy;
		for(int i=0; i<NO_POINTS; i++) {
			for(int j=0; j<=i; j++) {
				if(i==j) {
					dist[i][j] = 0.0;
				}
				else {
					dx = x[i] - x[j];
					dy = y[i] - y[j];
					dist[i][j] = Math.sqrt(dx*dx+dy*dy);
					dist[j][i] = dist[i][j];
				}
			}
		}
	}
	
	public static void printDistances() {
		for(int i=0; i<NO_POINTS; i++) {
			for(int j=0; j<NO_POINTS; j++) {
				System.out.printf("%14.4f",dist[i][j]);
			}
			System.out.println();
		}
	}

}
